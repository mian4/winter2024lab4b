public class Animal{
	private String food;
	private double size;
	private boolean flies;
	
	public Animal(String food, double size, boolean flies){
		this.food = food;
		this.size = size;
		this.flies = flies;
	}
	
	public String favFood(){
		return "Hi I'm a dragon and I like " + food + ".";
	}
	
	public String fliesOrNot(){
		if (flies == true)
			return "Hi I'm a dragon and I can fly :)";
		else
			return "Hi I'm a dragon and I can't fly :( ";
	}
	public double getSize(){
		return this.size;
	}
	public String getFood(){
		return this.food;
	}
	public boolean getFlies(){
		return this.flies;
	}
	public void setFlies(boolean flies){
		this.flies = flies;
	}
	
}