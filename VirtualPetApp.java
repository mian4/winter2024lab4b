import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		
		Scanner reader = new Scanner(System.in);
		
		Animal[] animals = new Animal[1];
		
		for (int i = 0; i < animals.length; i++){
			System.out.println("What food does it eat?");
			String food = reader.nextLine();
			
			System.out.println("Whats its size in feet ?");
			double size = reader.nextDouble();
			
			System.out.println("Does it fly? true or false");
			boolean flies = reader.nextBoolean();
			reader.nextLine();
			animals[i] = new Animal(food, size, flies);	
		}
		System.out.println("The flies is "+animals[animals.length-1].getFlies());
		System.out.println("The size is "+animals[animals.length-1].getSize());
		System.out.println("The food is "+animals[animals.length-1].getFood());
		System.out.println("Change the Flies value to the contrary please");
		boolean newFlies = reader.nextBoolean();
		animals[animals.length-1].setFlies(newFlies);
		System.out.println("The flies is "+animals[animals.length-1].getFlies());
		System.out.println("The size is "+animals[animals.length-1].getSize());
		System.out.println("The food is "+animals[animals.length-1].getFood());
	}
}